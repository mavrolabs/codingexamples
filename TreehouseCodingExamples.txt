

-----------------------------------------------------------------
iOS RetroLabel2600 and MyPix Information Table row select actions
-----------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableMoreOptions)
    {   
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                // Tell A Friend
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"TellAFriendWithTitle", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Email", nil), NSLocalizedString(@"Facebook", nil), NSLocalizedString(@"Twitter", nil), nil];
                actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
                actionSheet.tag = 2;
                [actionSheet showInView:self.view];
            }
            
            if (indexPath.row == 1)
            {
                // Rate This Application
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ReviewTitle", nil) 
                                                                message:NSLocalizedString(@"ReviewMessage", nil) 
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ReviewNo", nil) 
                                                      otherButtonTitles:NSLocalizedString(@"ReviewYes", nil), nil];
                alert.tag = 2;
                
                [alert show];
                
                [tableView deselectRowAtIndexPath:indexPath animated:NO];
                
                return;
                
            }
            
            if (indexPath.row == 2)
            {
               <...>


----------------------------------------
Android Information panel select actions
----------------------------------------

protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		setTitle("About RetroLabel 2600");
		
		if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
	        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	    else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
	        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    } 
	    else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
	        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
	    else {
	    	setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    }
		
		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/bauhausdemi.ttf");
		((TextView)findViewById(R.id.textViewAboutTitle)).setTypeface(tf, Typeface.NORMAL);
		((TextView)findViewById(R.id.textViewAboutLinks)).setTypeface(tf, Typeface.NORMAL);
		((TextView)findViewById(R.id.aboutCopyright)).setTypeface(tf, Typeface.NORMAL);
		
		((Button)findViewById(R.id.buttonTellAFriend)).setTypeface(tf, Typeface.NORMAL);
		((Button)findViewById(R.id.buttonSupport)).setTypeface(tf, Typeface.NORMAL);
		((Button)findViewById(R.id.buttonVisitRL26K)).setTypeface(tf, Typeface.NORMAL);
		((Button)findViewById(R.id.buttonVisitMavroLabs)).setTypeface(tf, Typeface.NORMAL);
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		((TextView)findViewById(R.id.aboutCopyright)).setText(String.format(copyright, year, version));
		
		Button buttonTellAFriend = (Button) findViewById(R.id.buttonTellAFriend);
		buttonTellAFriend.setOnClickListener(new OnClickListener() {
        	@Override
    		public void onClick(View arg0) {
        		Intent intent = new Intent(Intent.ACTION_SEND);
        		intent.setType("plain/text");
        		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
        		intent.putExtra(Intent.EXTRA_SUBJECT, "RetroLabel 2600 for Android");
        		intent.putExtra(Intent.EXTRA_TEXT, "Check out the RetroLabel 2600 app for Android.\n\nVisit http://www.retrolabel2600.com for more information.");
        		startActivity(Intent.createChooser(intent, ""));
        	}
        });
		
		Button buttonSupport = (Button) findViewById(R.id.buttonSupport);
		buttonSupport.setOnClickListener(new OnClickListener() {
        	@Override
    		public void onClick(View arg0) {
        		String androidVersion = android.os.Build.VERSION.RELEASE; // e.g. myVersion := "1.6"
        		int sdkVersion = android.os.Build.VERSION.SDK_INT; // e.g. sdkVersion := 8; 
        		String model = android.os.Build.MODEL;
        		String modelName = android.os.Build.MANUFACTURER + android.os.Build.PRODUCT;
        		
        		Intent intent = new Intent(Intent.ACTION_SEND);
        		intent.setType("plain/text");
        		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "support@mavrolabs.com" });
        		intent.putExtra(Intent.EXTRA_SUBJECT, "Support/Suggestion from RetroLabel 2600 Android App");
        		intent.putExtra(Intent.EXTRA_TEXT, String.format(osInfo, version, androidVersion, sdkVersion, model, modelName));
        		startActivity(Intent.createChooser(intent, ""));
        	}
        });
		
		Button buttonVisitRL26K = (Button) findViewById(R.id.buttonVisitRL26K);
		buttonVisitRL26K.setOnClickListener(new OnClickListener() {
        	@Override
    		public void onClick(View arg0) {
        		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.retrolabel2600.com"));
        		startActivity(browserIntent);
        	}
        });
		
		Button buttonVisitMavroLabs = (Button) findViewById(R.id.buttonVisitMavroLabs);
		buttonVisitMavroLabs.setOnClickListener(new OnClickListener() {
        	@Override
    		public void onClick(View arg0) {
        		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mavrolabs.com"));
        		startActivity(browserIntent);
        	}
        });

	}
